import unittest
import app
BASE_URL = 'http://127.0.0.1:5000/'
class TestFlaskApi(unittest.TestCase):
    def setUp(self):
        self.app = app.app.test_client()
        self.app.testing = True
    def test_get_root(self):
        response = self.app.get(BASE_URL)
        data = response.get_data()
        # TODO
        print(data)
        self.assertEqual(response.status_code, 200)
if __name__ == "__main__":
    unittest.main()
